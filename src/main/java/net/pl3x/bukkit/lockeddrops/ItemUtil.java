package net.pl3x.bukkit.lockeddrops;

import org.bukkit.entity.Item;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

public class ItemUtil {
    public static void setOwner(Item item, String uuid) {
        item.setMetadata("owner", new FixedMetadataValue(LockedDrops.getPlugin(), uuid));
        Logger.debug("[ItemUtil.setOwner] Owner set " + item.toString() + " (" + item.getUniqueId() + ") to " + uuid);
    }

    public static String getOwner(Item item) {
        if (!item.hasMetadata("owner")) {
            Logger.debug("[ItemUtil.getOwner] No owner metadata");
            return null; // no metadata
        }

        List<MetadataValue> meta = item.getMetadata("owner");
        if (meta.size() == 0) {
            Logger.debug("[ItemUtil.getOwner] Size 0");
            return null; // no metadata
        }

        MetadataValue metaValue = meta.get(0);
        if (metaValue == null) {
            Logger.debug("[ItemUtil.getOwner] Meta value null");
            return null; // no metadata
        }

        return metaValue.asString();
    }
}
