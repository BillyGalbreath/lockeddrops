package net.pl3x.bukkit.lockeddrops;

import net.pl3x.bukkit.lockeddrops.command.CmdLockedDrops;
import net.pl3x.bukkit.lockeddrops.configuration.Config;
import net.pl3x.bukkit.lockeddrops.configuration.Lang;
import net.pl3x.bukkit.lockeddrops.drop.DropWatcher;
import net.pl3x.bukkit.lockeddrops.listener.DropListener;
import net.pl3x.bukkit.lockeddrops.listener.ItemListener;
import net.pl3x.bukkit.lockeddrops.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class LockedDrops extends JavaPlugin {
    private DropWatcher dropWatcher;

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        dropWatcher = new DropWatcher(this);

        Bukkit.getPluginManager().registerEvents(new DropListener(this), this);
        Bukkit.getPluginManager().registerEvents(new ItemListener(this), this);
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);

        getCommand("lockeddrops").setExecutor(new CmdLockedDrops(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static LockedDrops getPlugin() {
        return LockedDrops.getPlugin(LockedDrops.class);
    }

    public DropWatcher getDropWatcher() {
        return dropWatcher;
    }
}
