package net.pl3x.bukkit.lockeddrops.command;

import net.pl3x.bukkit.lockeddrops.Chat;
import net.pl3x.bukkit.lockeddrops.LockedDrops;
import net.pl3x.bukkit.lockeddrops.Logger;
import net.pl3x.bukkit.lockeddrops.configuration.Config;
import net.pl3x.bukkit.lockeddrops.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.Collections;
import java.util.List;

public class CmdLockedDrops implements TabExecutor {
    private final LockedDrops plugin;

    public CmdLockedDrops(LockedDrops plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase())) {
            return Collections.singletonList("reload");
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.lockeddrops")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            Logger.debug("Reloading config...");
            Config.reload();

            Logger.debug("Reloading language file...");
            Lang.reload();

            new Chat(Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()))
                    .send(sender);
            return true;
        }

        new Chat(Lang.VERSION
                .replace("{version}", plugin.getDescription().getVersion())
                .replace("{plugin}", plugin.getName()))
                .send(sender);
        return true;
    }
}
