package net.pl3x.bukkit.lockeddrops.configuration;

import net.pl3x.bukkit.lockeddrops.LockedDrops;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static int LOCK_DROPS_TIME = 30;
    public static List<String> DISABLED_WORLDS = new ArrayList<>();

    public static void reload() {
        LockedDrops plugin = LockedDrops.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        LOCK_DROPS_TIME = config.getInt("lock-drops-time", 30);
        DISABLED_WORLDS = config.getStringList("disabled-worlds");
    }

    public static boolean isWorldDisabled(World world) {
        if (world == null) {
            return true;
        }
        String worldName = world.getName();
        for (String disabled : DISABLED_WORLDS) {
            if (disabled.equalsIgnoreCase(worldName)) {
                return true;
            }
        }
        return false;
    }
}
