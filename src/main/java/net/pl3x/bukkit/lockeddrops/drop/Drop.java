package net.pl3x.bukkit.lockeddrops.drop;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class Drop {
    private final Material material;
    private final byte data;
    private final long tick;
    private final Location location;
    private final UUID player;

    public Drop(ItemStack stack, long tick, Location location, UUID player) {
        this.material = stack.getType();
        //noinspection deprecation
        this.data = stack.getData().getData();
        this.tick = tick;
        this.player = player;

        // clean up location to block location
        this.location = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public Material getMaterial() {
        return material;
    }

    public byte getData() {
        return data;
    }

    public long getTick() {
        return tick;
    }

    public Location getLocation() {
        return location;
    }

    public UUID getPlayerUUID() {
        return player;
    }

    @Override
    public String toString() {
        return "ProtectDrop[" +
                "material: " + material + ", " +
                "data: " + data + ", " +
                "tick: " + tick + ", " +
                "location: " + location + ", " +
                "player: " + player + "]";
    }
}
