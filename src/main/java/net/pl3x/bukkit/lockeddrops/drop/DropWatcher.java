package net.pl3x.bukkit.lockeddrops.drop;

import net.pl3x.bukkit.lockeddrops.LockedDrops;
import net.pl3x.bukkit.lockeddrops.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.HashSet;

public class DropWatcher {
    private final LockedDrops plugin;
    private final Collection<Drop> drops = new HashSet<>();

    public DropWatcher(LockedDrops plugin) {
        this.plugin = plugin;
    }

    public void watchDrop(Collection<Drop> drop) {
        if (drop == null || drop.isEmpty()) {
            return; // nothing to watch
        }

        drops.addAll(drop);

        Logger.debug("[watchDrop] Watching drop");

        // stop watching after 2 ticks
        Bukkit.getScheduler().runTaskLater(plugin,
                () -> unwatchDrop(drop), 2);
    }

    public void unwatchDrop(Collection<Drop> drop) {
        Logger.debug("[unwatchDrop] Stop watching drop");

        drops.removeAll(drop);
    }

    public Drop getDrop(Item item) {
        long tick = item.getWorld().getFullTime();

        ItemStack stack = item.getItemStack();
        Location itemLoc = item.getLocation();
        String itemWorld = itemLoc.getWorld().getName();

        for (Drop drop : drops) {
            if (drop.getMaterial() != stack.getType()) {
                Logger.debug("[getDrop] " + drop.getMaterial() + " != " + stack.getType());
                continue; // not same material
            }
            //noinspection deprecation
            if (drop.getData() != stack.getData().getData()) {
                //noinspection deprecation
                Logger.debug("[getDrop] " + drop.getData() + " != " + stack.getData().getData());
                continue; // not same data/durability
            }
            if (drop.getTick() < tick - 1 || drop.getTick() > tick + 1) {
                Logger.debug("[getDrop] " + drop.getTick() + " < " + tick + " - 1 || " + drop.getTick() + " > " + tick + " + 1");
                continue; // was not within timeframe
            }
            Location dropLoc = drop.getLocation();
            if (!dropLoc.getWorld().getName().equals(itemWorld)) {
                Logger.debug("[getDrop] !" + dropLoc.getWorld().getName() + ".equals(" + itemWorld + ")");
                continue; // not in the same world
            }
            if (dropLoc.distanceSquared(itemLoc) > 9) {
                Logger.debug("[getDrop] " + dropLoc.distanceSquared(itemLoc) + " > 9");
                continue; // farther than 3 blocks
            }

            Logger.debug("[getDrop] Drop found " + drop);
            return drop;
        }

        Logger.debug("[getDrop] NULL");
        return null;
    }

}
