package net.pl3x.bukkit.lockeddrops.listener;

import net.pl3x.bukkit.lockeddrops.LockedDrops;
import net.pl3x.bukkit.lockeddrops.Logger;
import net.pl3x.bukkit.lockeddrops.configuration.Config;
import net.pl3x.bukkit.lockeddrops.drop.Drop;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

public class DropListener implements Listener {
    private final LockedDrops plugin;

    public DropListener(LockedDrops plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (Config.LOCK_DROPS_TIME <= 0 || Config.isWorldDisabled(event.getBlock().getWorld())) {
            Logger.debug("[BlockBreakEvent] disabled.");
            return; // disabled
        }

        // get event info
        Block block = event.getBlock();
        Location location = block.getLocation();
        Player player = event.getPlayer();

        // get possible items from API
        Collection<ItemStack> stacks = block.getDrops();

        // add items obtainable using special tool
        ItemStack hand;
        try {
            hand = player.getInventory().getItemInMainHand();
        } catch (Exception e) {
            //noinspection deprecation
            hand = player.getItemInHand();
        }
        stacks.addAll(block.getDrops(hand));

        // add special items
        if (block.getState() instanceof Banner) {
            // for banners we want ink_sack color (not wool color)
            //noinspection deprecation
            stacks.add(new ItemStack(Material.BANNER, ((Banner) block.getState()).getBaseColor().getDyeData()));
        }

        // add the block itself (for silk touch)
        //noinspection deprecation
        stacks.add(new ItemStack(block.getType(), block.getData()));

        Logger.debug("[BlockBreakEvent] Watching " + block.getLocation());

        // watch the drops
        plugin.getDropWatcher().watchDrop(stacks.stream()
                .map(stack -> new Drop(
                        stack,
                        player.getWorld().getFullTime(),
                        location,
                        player.getUniqueId()))
                .collect(Collectors.toCollection(HashSet::new)));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onHangingBreakByPlayer(HangingBreakByEntityEvent event) {
        if (!(event.getRemover() instanceof Player)) {
            Logger.debug("[HangingBreakByEntityEvent] not broke by player.");
            return; // not broke by player
        }

        Hanging hanging = event.getEntity();
        if (Config.LOCK_DROPS_TIME <= 0 || Config.isWorldDisabled(hanging.getWorld())) {
            Logger.debug("[HangingBreakByEntityEvent] disabled.");
            return; // disabled
        }

        // get drop
        Collection<ItemStack> stacks = new HashSet<>();
        switch (hanging.getType()) {
            case PAINTING:
                stacks.add(new ItemStack(Material.PAINTING));
                break;
            case ITEM_FRAME:
                stacks.add(new ItemStack(Material.ITEM_FRAME));
                break;
            case LEASH_HITCH:
                stacks.add(new ItemStack(Material.LEASH));
                break;
            default:
                return; // should not happen
        }

        Logger.debug("[HangingBreakByEntityEvent] Watching " + hanging.getLocation());

        // watch the drops
        plugin.getDropWatcher().watchDrop(stacks.stream()
                .map(stack -> new Drop(
                        stack,
                        hanging.getWorld().getFullTime(),
                        hanging.getLocation(),
                        event.getRemover().getUniqueId()))
                .collect(Collectors.toCollection(HashSet::new)));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerBreakArmorStand(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) {
            Logger.debug("[EntityDamageByEntityEvent] not player");
            return; // not damaged by a player
        }

        Entity armorstand = event.getEntity();
        if (!armorstand.getType().equals(EntityType.ARMOR_STAND)) {
            Logger.debug("[EntityDamageByEntityEvent] not armorstand");
            return; // not an armor stand
        }

        if (Config.LOCK_DROPS_TIME <= 0 || Config.isWorldDisabled(armorstand.getWorld())) {
            Logger.debug("[EntityDamageByEntityEvent] disabled");
            return; // disabled
        }

        // get armorstand
        Collection<ItemStack> stacks = new HashSet<>();
        stacks.add(new ItemStack(Material.ARMOR_STAND));

        // get armorstand contents
        ItemStack boots = ((ArmorStand) armorstand).getBoots();
        ItemStack leggings = ((ArmorStand) armorstand).getLeggings();
        ItemStack chestplate = ((ArmorStand) armorstand).getChestplate();
        ItemStack helmet = ((ArmorStand) armorstand).getHelmet();
        ItemStack hand = ((ArmorStand) armorstand).getItemInHand();
        if (boots != null) {
            stacks.add(boots);
        }
        if (leggings != null) {
            stacks.add(leggings);
        }
        if (chestplate != null) {
            stacks.add(chestplate);
        }
        if (helmet != null) {
            stacks.add(helmet);
        }
        if (hand != null) {
            stacks.add(hand);
        }

        Logger.debug("[EntityDamageByEntityEvent] Watching " + armorstand.getLocation());

        // watch the drops
        plugin.getDropWatcher().watchDrop(stacks.stream()
                .map(stack -> new Drop(
                        stack,
                        armorstand.getWorld().getFullTime(),
                        armorstand.getLocation(),
                        event.getDamager().getUniqueId()))
                .collect(Collectors.toCollection(HashSet::new)));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDestroyVehicle(VehicleDestroyEvent event) {
        Entity vehicle = event.getVehicle();
        if (Config.LOCK_DROPS_TIME <= 0 || Config.isWorldDisabled(vehicle.getWorld())) {
            Logger.debug("[VehicleDestroyEvent] disabled");
            return; // disabled
        }

        Entity attacker = event.getAttacker();
        if (!(attacker instanceof Player)) {
            if (!(attacker instanceof Projectile)) {
                Logger.debug("[VehicleDestroyEvent] not player or projectile");
                return; // not a player or projectile
            }

            ProjectileSource shooter = ((Projectile) attacker).getShooter();
            if (!(shooter instanceof Player)) {
                Logger.debug("[VehicleDestroyEvent] not player owned projectile");
            }
        }

        // get the pieces broken from vehicle
        Collection<ItemStack> stacks = new HashSet<>();
        switch (vehicle.getType()) {
            case BOAT:
                stacks.add(new ItemStack(Material.BOAT));
                break;
            case MINECART_CHEST:
                stacks.add(new ItemStack(Material.MINECART));
                stacks.add(new ItemStack(Material.CHEST));
                break;
            case MINECART_COMMAND:
                stacks.add(new ItemStack(Material.MINECART));
                stacks.add(new ItemStack(Material.COMMAND));
                break;
            case MINECART_FURNACE:
                stacks.add(new ItemStack(Material.MINECART));
                stacks.add(new ItemStack(Material.FURNACE));
                stacks.add(new ItemStack(Material.BURNING_FURNACE));
                break;
            case MINECART_HOPPER:
                stacks.add(new ItemStack(Material.MINECART));
                stacks.add(new ItemStack(Material.HOPPER));
                break;
            case MINECART_MOB_SPAWNER:
                stacks.add(new ItemStack(Material.MINECART));
                stacks.add(new ItemStack(Material.MOB_SPAWNER));
                break;
            case MINECART_TNT:
                stacks.add(new ItemStack(Material.MINECART));
                stacks.add(new ItemStack(Material.TNT));
                break;
            case MINECART:
                stacks.add(new ItemStack(Material.MINECART));
                break;
            default:
                return; // should not happen
        }

        if (stacks.isEmpty()) {
            Logger.debug("[VehicleDestroyEvent] Nothing to lock");
            return; // nothing to lock
        }

        Logger.debug("[VehicleDestroyEvent] Watching " + vehicle.getLocation());

        // watch the drops
        plugin.getDropWatcher().watchDrop(stacks.stream()
                .map(stack -> new Drop(
                        stack,
                        vehicle.getWorld().getFullTime(),
                        vehicle.getLocation(),
                        attacker.getUniqueId()))
                .collect(Collectors.toCollection(HashSet::new)));
    }
}
