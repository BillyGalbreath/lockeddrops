package net.pl3x.bukkit.lockeddrops.listener;

import net.pl3x.bukkit.lockeddrops.ItemUtil;
import net.pl3x.bukkit.lockeddrops.LockedDrops;
import net.pl3x.bukkit.lockeddrops.Logger;
import net.pl3x.bukkit.lockeddrops.configuration.Config;
import net.pl3x.bukkit.lockeddrops.drop.Drop;
import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;

public class ItemListener implements Listener {
    private final LockedDrops plugin;

    public ItemListener(LockedDrops plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemSpawn(ItemSpawnEvent event) {
        // get spawned item
        Item item = event.getEntity();
        if (Config.LOCK_DROPS_TIME <= 0 || Config.isWorldDisabled(item.getWorld())) {
            Logger.debug("[ItemSpawnEvent] disabled.");
            return; // disabled
        }

        // get watched drop
        Drop drop = plugin.getDropWatcher().getDrop(item);
        if (drop == null) {
            Logger.debug("[ItemSpawnEvent] drop is null");
            return; // this item is not watched
        }

        // lock the item immediately
        ItemUtil.setOwner(item, drop.getPlayerUUID().toString());
        Logger.debug("[ItemSpawnEvent] Locked " + item.toString() + " (" + item.getUniqueId() + ") to " + drop.getPlayerUUID());

        // unlock after configured time passes
        Bukkit.getScheduler().runTaskLater(plugin,
                () -> {
                    Logger.debug("[ItemSpawnEvent] Unlocked " + item.toString() + " (" + item.getUniqueId() + ")");
                    ItemUtil.setOwner(item, "none");
                },
                Config.LOCK_DROPS_TIME * 20);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemMerge(ItemMergeEvent event) {
        Item item = event.getEntity();
        if (Config.LOCK_DROPS_TIME <= 0 || Config.isWorldDisabled(item.getWorld())) {
            Logger.debug("[ItemMergeEvent] disabled.");
            return; // disabled
        }

        // get locked owners
        String itemOwner = ItemUtil.getOwner(item);
        String targetOwner = ItemUtil.getOwner(event.getTarget());

        if (itemOwner == null && targetOwner == null) {
            return; // both not locked, let merge
        }

        if (itemOwner == null || !itemOwner.equals(targetOwner)) {
            event.setCancelled(true); // owners do not match, do not merge
        }
    }
}
