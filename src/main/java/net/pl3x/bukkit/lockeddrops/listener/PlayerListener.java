package net.pl3x.bukkit.lockeddrops.listener;

import net.pl3x.bukkit.lockeddrops.ItemUtil;
import net.pl3x.bukkit.lockeddrops.Logger;
import net.pl3x.bukkit.lockeddrops.configuration.Config;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PlayerListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        Item item = event.getItem();
        if (Config.LOCK_DROPS_TIME <= 0 || Config.isWorldDisabled(item.getWorld())) {
            Logger.debug("[PlayerPickupItemEvent] disabled");
            return; // disabled
        }

        String owner = ItemUtil.getOwner(item);
        if (owner == null || owner.isEmpty() || owner.equalsIgnoreCase("none")) {
            Logger.debug("[PlayerPickupItemEvent] Not locked anymore " + item.toString() + "(" + item.getUniqueId() + ")");
            return; // owner removed and/or not locked
        }

        String uuid = event.getPlayer().getUniqueId().toString();
        if (owner.equals(uuid)) {
            Logger.debug("[PlayerPickupItemEvent] Owner picked up " + item.toString() + "(" + item.getUniqueId() + ")");
            return; // this is owner. allow to pick up
        }

        // item locked, not owner, cancel pickup event
        Logger.debug("[PlayerPickupItemEvent] " + uuid + " is not owner, cant pickup! " + item.toString() + "(" + item.getUniqueId() + ")");
        event.setCancelled(true);
    }
}
